# CF2 with Response Information Failure Detection (ISVC 2016)

### Introduction

This is the research code and results for the paper:

Walsh, R., Medeiros, H. “Detecting Tracking Failures From Correlation Response Maps,” International Symposium on Visual Computing, 2016.

* [PDF](http://coviss.org/wp-content/uploads/2016/10/Walsh2016Detecting.pdf)

CF2 with Response Information Failure Detection (RFD-CF2) is a modified version of the Correlation Filters with Convolutional Features ([CF2](https://github.com/jbhuang0604/CF2)) tracker extended with a failure detection module.

### Abstract
Tracking methods based on correlation filters have gained popularity in recent years due to their robustness to rotations, occlusions, and other challenging aspects of visual tracking. Such methods generate a confidence or response map which is used to estimate the new location of the tracked target. However, by examining the features of this map, important details about the tracker status can be inferred and compensatory measures can be taken in order to minimize failures. We propose an algorithm that uses the mean and entropy of this response map to prevent bad target model updates caused by problems such as occlusions and motion blur as well as to determine the size of the target search area. Quantitative experiments demonstrate that our method improves success plots over a baseline tracker that does not incorporate our failure detection mechanism.

### Quick Setup Guide

1. Download the [mat file](http://www.vlfeat.org/matconvnet/models/imagenet-vgg-verydeep-19.mat) for [VGG-NET-19](http://www.vlfeat.org/matconvnet/pretrained/) and place it in the model folder.
2. Download [MatConvNet](http://www.vlfeat.org/matconvnet/)
We used [1.0-beta20](http://www.vlfeat.org/matconvnet/download/matconvnet-1.0-beta20.tar.gz). If you are using another version, please ensure version comparability.
3. Setup MatCovNet by including the path of matconvnet/matlab.
For us, this looked like...
```matlab
addpath('~/SourceCode/matconvnet-1.0-beta20/matlab');
vl_setupnn
vl_compilenn
```
4. Run the main entry file run_tracker.m

### OTB-50 and OTB-100 Interface Setup Guide

1. Download the [Visual Tracking Benchmark](http://cvlab.hanyang.ac.kr/tracker_benchmark) and unpack to a directory of your choice.
2. Add the RFD-CF2 tracker folder to the benchmark's tracking folder.
3. Update the configTrackers.m file inside the util folder.
This should look something like...
```matlab
function trackers=configTrackers

...

myTrackerTestSet={   
	struct('name', 'RFD_CF2', 'namePaper', 'RFD-CF2'), ...
	struct('name', 'Tacker2FileName', 'namePaper', 'Tracker2DisplayName'), ...
	struct('name', 'Tacker3FileName', 'namePaper', 'Tracker3DisplayName')
};

...

trackers = [myTrackerTestSet];
```
4. Run main_running.m to run the benchmark. You should see the benchmark updating with a visual window. Run perfPlot.m to to generate graphs.

###VOT2015 and VOT2016 Interface Setup Guide

**Known Issue:**
The run_RFD_CF2_VOT file does not generate the same results as the paper. Adding comments, changing variable names, and changing the filename appears to change results with consistency. The results we provide are the same for the paper, and the original, uncommented code for the VOT implementation is included in the folder, originalFiles. Also, because of issues with the toolkit at the time, we used the [vot2015-final](https://github.com/votchallenge/vot-toolkit/releases/tag/vot2015-final) release.

1. Download the [VOT Benchmark](http://www.votchallenge.net/howto) and unpack to a directory of your choice using instructions. We named our tracker RFD_CF2 and the tracker interpreter is matlab.
2. Add the RFD-CF2 tracker folder inside your VOT workspace folder as given in the VOT tutorial.
3. Add the vot.m file from the VOT package into the RFD-CF2 tracker folder as indicated in the VOT tutorial.	
4. Configure the tracker_RFD_CF2.m file. You can copy the following over the existing file. Notice that the path to your VOT workspace may differ.
```matlab
% The human readable label for the tracker, used to identify the tracker in reports
% If not set, it will be set to the same value as the identifier.
% It does not have to be unique, but it is best that it is.
tracker_label = 'RFD-CF2';

% For MATLAB implementations we have created a handy function that generates the appropriate
% command that will run the matlab executable and execute the given script that includes your
% tracker implementation.
%
% Please customize the line below by substituting the first argument with the name of the
% script (not the .m file but just the name of the script as you would use it in within Matlab)
% of your tracker and also provide the path (or multiple paths) where the tracker sources
% are found as the elements of the cell array (second argument).

tracker_command = generate_matlab_command('run_RFD_CF2_VOT',...
   {'~/Documents/MATLAB/VOT2016/vot-workspace/tracker_folder_RFD_CF2',...
    '~/Documents/MATLAB/VOT2016/vot-workspace/tracker_folder_RFD_CF2/utility',...
    '~/Documents/MATLAB/VOT2016/vot-workspace/tracker_folder_RFD_CF2/model',...
    '~/Documents/MATLAB/VOT2016/vot-workspace/tracker_folder_RFD_CF2/external/matconvnet/matlab',...
    '~/Documents/MATLAB/VOT2016/vot-workspace/tracker_folder_RFD_CF2/external/matconvnet/matlab/mex',...
    '~/Documents/MATLAB/VOT2016/vot-workspace/tracker_folder_RFD_CF2/external/matconvnet/matlab/xtest'});

tracker_interpreter = 'matlab';
```
5. Run run_test.m to confirm that your setup worked. In the menu, select "run tracker once within the evaluation" and any sequence you desire. The program should, after a while, give you the option to "visually compare results with the groundtruth".

### Results on tracking benchmarks

* One-pass evaluation (OPE) on the 50 tracking sequences in [Wu et al. CVPR 2013](https://sites.google.com/site/trackerbenchmark/benchmarks/v10)
* Spatial robustness evaluation (SRE) on the 50 tracking sequences in [Wu et al. CVPR 2013](https://sites.google.com/site/trackerbenchmark/benchmarks/v10)
* Temporal robustness evaluation (TRE) on the 50 tracking sequences in [Wu et al. CVPR 2013](https://sites.google.com/site/trackerbenchmark/benchmarks/v10)
* One-pass evaluation (OPE) on the 100 tracking sequences in [Wu et al. PAMI 2015](https://sites.google.com/site/benchmarkpami) 
* Spatial robustness evaluation (SRE) on the 100 tracking sequences in [Wu et al. PAMI 2015](https://sites.google.com/site/benchmarkpami)
* Temporal robustness evaluation (TRE) on the 100 tracking sequences in [Wu et al. PAMI 2015](https://sites.google.com/site/benchmarkpami)
* Accuracy and robustness evaluation on the 60 tracking sequences in [Kristan et al. ICCV 2015](http://www.votchallenge.net/vot2015/dataset.html)

### Citation

    @inproceedings{Walsh2016Detecting,
        title={Detecting Tracking Failures From Correlation Response Maps},
        Author = {Walsh, Ryan and Medeiros, Henry},
        booktitle = {International Symposium on Visual Computing},
        pages={125--135},
        Year = {2016},
        Organization={Springer}
    }

For feedback, contact [ryan.w.walsh@marquette.edu](mailto:ryan.w.walsh@marquette.edu), or [henry.medeiros@marquette.edu](mailto:henry.medeiros@marquette.edu)