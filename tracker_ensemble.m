% tracker_ensemble: RFD-CF2
%
% Input:
%   - video_path:               path to the image sequence
%   - img_files:                list of image names
%   - pos:                      initialized center position of the target in (row, col)
%   - target_sz:                initialized target size in (Height, Width)
% 	- padding:                  padding parameter for the search area
%   - lambda:                   regularization term for ridge regression
%   - output_sigma_factor:      spatial bandwidth for the Gaussian label
%   - interp_factor:            learning rate for model update
%   - cell_size:                spatial quantization level
%   - show_visualization:       set to True for showing intermediate results
%   - debug_bool:               set to True for showing RFD-CF2 data visualization
%   - max_list_size:            raise to prevent tracker from learning too fast
%   - mean_thresh:              mean thresholding between (any loss/found)
%   - entropy_thresh_full:      entropy threshold between (partial loss/full loss)
%   - entropy_thresh_partial:   entropy threshold between (partial loss/found)
% Output:
%   - positions:                predicted target position at each frame
%   - time:                     time spent for tracking
%
% Citations and References:
%   CF2 Citation:
%   Hierarchical Convolutional Features for Visual Tracking
%   Chao Ma, Jia-Bin Huang, Xiaokang Yang, and Ming-Hsuan Yang
%   IEEE International Conference on Computer Vision, ICCV 2015
%
%   Our Paper:
%	Detecting Tracking Failures From Correlation Response Maps
%	Walsh, Ryan and Medeiros, Henry	
%	International Symposium on Visual Computing, ISVC 2016
%
%   Contact For CF2 Base Code:
%   Chao Ma (chaoma99@gmail.com), or
%   Jia-Bin Huang (jbhuang1@illinois.edu)
%
%   Contact For RFD-CF2:
%   Ryan Walsh (ryan.w.walsh@marquette.edu), or
%   Henry Medeiors (henry.medeiros@marquette.edu)

function [positions, time] = tracker_ensemble(video_path, img_files, pos, target_sz, ...
    lambda, output_sigma_factor, interp_factor, cell_size, show_visualization, debug_bool, ...
    max_list_size, mean_thresh, entropy_thresh_full, entropy_thresh_partial, padding_original, scale_min, scale_max)

padding = padding_original;

% ================================================================================
% Environment setting
% ================================================================================

indLayers = [37, 28, 19];   % The CNN layers Conv5-4, Conv4-4, and Conv3-4 in VGG Net
nweights  = [1, 0.5, 0.02]; % Weights for combining correlation filter responses
numLayers = length(indLayers);

% Get image size and search window size
im_sz     = size(imread([video_path img_files{1}]));
window_sz = get_search_window(target_sz, im_sz, padding);

% Compute the sigma for the Gaussian function label
output_sigma = sqrt(prod(target_sz)) * output_sigma_factor / cell_size;

%create regression labels, gaussian shaped, with a bandwidth
%proportional to target size    d=bsxfun(@times,c,[1 2]);

l1_patch_num = floor(window_sz/ cell_size);

% Pre-compute the Fourier Transform of the Gaussian function label
yf = fft2(gaussian_shaped_labels(output_sigma, l1_patch_num));

% Pre-compute and cache the cosine window (for avoiding boundary discontinuity)
cos_window = hann(size(yf,1)) * hann(size(yf,2))';

% Create video interface for visualization
if(show_visualization)
    update_visualization = show_video(img_files, video_path, entropy_thresh_full, entropy_thresh_partial, debug_bool);
end

% Initialize variables for calculating FPS and distance precision
time      = 0;
positions = zeros(numel(img_files), 2);
nweights  = reshape(nweights,1,1,[]);

% Note: variables ending with 'f' are in the Fourier domain.
model_xf     = cell(1, numLayers);
model_alphaf = cell(1, numLayers);

% Declare variables for use in occlusion detection
mean_list       = [];
entropy_list    = [];
response_flag   = 0;
sacle_range     = scale_max-scale_min;

% Declare variables for RFD-CF2 data visualization
visualPkg = cell(5,1); %Varibles to be printed to debug UI stored here before being send off to update_visualization

% ================================================================================
% Start tracking
% ================================================================================
for frame = 1:numel(img_files)
    im = imread([video_path img_files{frame}]); % Load the image at the current frame
    if ismatrix(im)
        im = cat(3, im, im, im);
    end
    
    tic();
    % ================================================================================
    % Predicting the object position from the learned object model
    % ================================================================================
    if frame > 1
        % Extracting hierarchical convolutional features
        feat = extractFeature(im, pos, window_sz, cos_window, indLayers);
        % Predict position
        [pos, mean_list, entropy_list, response_flag, padding, visualPkg] = predictPosition(feat, pos, indLayers, nweights, cell_size, l1_patch_num, ...
            model_xf, model_alphaf, mean_list, entropy_list, max_list_size, mean_thresh, entropy_thresh_full, entropy_thresh_partial, response_flag, padding_original, scale_min, sacle_range);
        window_sz = get_search_window(target_sz, im_sz, padding);
    else
        visualPkg{1} = 0; % {valid bit, response mat, normalized mean, normalized entropy}
    end
    
    % ================================================================================
    % Learning correlation filters over hierarchical convolutional features
    % ================================================================================
    % Extracting hierarchical convolutional features
    feat  = extractFeature(im, pos, window_sz, cos_window, indLayers);
    % Model update
    
    if response_flag == 1 || response_flag==0
        [model_xf, model_alphaf] = updateModel(feat, yf, interp_factor, lambda, frame, model_xf, model_alphaf);
    end

    % ================================================================================
    % Save predicted position and timing
    % ================================================================================
    positions(frame,:) = pos;
    time = time + toc();
    
    % Visualization
    if show_visualization
        box = [pos([2,1]) - target_sz([2,1])/2, target_sz([2,1])]; % mark bounding box location
        visualPkg{5} = [pos([2,1]) - window_sz([2,1])/2, window_sz([2,1])]; % mark search window
        
        if response_flag == 2
            color='y';
        elseif response_flag == 3
            color='r';
        else
            color='g';
        end
        
        stop = update_visualization(frame, box, color, visualPkg);
        drawnow
        if stop, break, end  %user pressed Esc, stop early
        % pause(0.05)  % uncomment to run slower
    end
end
end



function [pos, mean_list, entropy_list, response_flag, padding, visualPkg] = predictPosition(feat, pos, indLayers, nweights, cell_size, l1_patch_num, ...
    model_xf, model_alphaf, mean_list, entropy_list, max_list_size, mean_thresh, entropy_thresh_full, entropy_thresh_partial, response_flag, padding_original, scale_min, scale_range)

    % ================================================================================
    % Compute correlation filter responses at each layer
    % ================================================================================
    res_layer = zeros([l1_patch_num, length(indLayers)]);

    for ii = 1 : length(indLayers)
        zf = fft2(feat{ii});
        kzf=sum(zf .* conj(model_xf{ii}), 3) / numel(zf);

        res_layer(:,:,ii) = real(fftshift(ifft2(model_alphaf{ii} .* kzf)));  %equation for fast detection
    end
    
    % Combine responses from multiple layers (see Eqn. 5)
    response = sum(bsxfun(@times, res_layer, nweights), 3);
    
    % ================================================================================
    % Find target location and Failure Detection
    % ================================================================================
    % Target location is at the maximum response. we must take into
    % account the fact that, if the target doesn't move, the peak
    % will appear at the top-left corner, not at the center (See KCF paper cited in CF2).
    % The responses wrap around cyclically.
    %
    % Failure detection is accomplished via computing a mean and entropy for
    % this response and thresholding them. The result of failure detection
    % is that the search space for computing the next response is altered,
    % online learning is toggled, and repositioning of the target is
    % toggled. (See RFD-CF2 paper)

    %Prepare and compute variables for failure detector
    response_mean = mean(response(:));
    response_entropy = entropy(imadjust(response(:)));
    
    %Find Mean Moving Average
    mean_list=[response_mean, mean_list];
    while size(mean_list, 2)>max_list_size+1
        mean_list=mean_list(1:end-1);
    end
    mean_moving_average=0;
    total=0;
    for i=1:1:size(mean_list, 2)
        mean_moving_average=mean_moving_average+mean_list(i)*i;
        total=total+i;
    end
    mean_moving_average=mean_moving_average/total;
    mean_difference = (mean_moving_average-response_mean)/((mean_moving_average+response_mean)/2);
    
    %Find Entropy Moving Average
    entropy_list=[response_entropy, entropy_list];
    while size(entropy_list, 2)>max_list_size+1
        entropy_list=entropy_list(1:end-1);
    end
    entropy_moving_average=0;
    total=0;
    for i=1:1:size(entropy_list, 2)
        entropy_moving_average=entropy_moving_average+entropy_list(i)*i;
        total=total+i;
    end
    entropy_moving_average=entropy_moving_average/total;
    entropy_difference = -(entropy_moving_average-response_entropy)/((entropy_moving_average+response_entropy)/2);
    
    % For window scaling, compute scaling factor for mean
    mean_scaling_factor = mean_difference/mean_thresh;
    if mean_scaling_factor<0
        mean_scaling_factor=0;
    elseif mean_scaling_factor>1
        mean_scaling_factor=1;
    end
    
    % For window scaling, compute scaling factor for entropy
    entropy_scaling_factor = entropy_difference/entropy_thresh_full;
    if entropy_scaling_factor<0
        entropy_scaling_factor=0;
    elseif mean_scaling_factor>1
        entropy_scaling_factor=1;
    end
    
    scalling=scale_range*((mean_difference/mean_thresh+entropy_difference/entropy_thresh_full)/2)*sqrt(entropy_scaling_factor*mean_scaling_factor)+scale_min;
    
    if scalling>(scale_min+scale_range)
        scalling=scale_min+scale_range;
    elseif scalling<scale_min
        scalling=scale_min;
    end
    
    % Set new window scaling
    padding = struct('generic', padding_original.generic*scalling, 'large', padding_original.large*scalling, 'height', padding_original.height*scalling);

    %Find failure detector state
    switch response_flag
        case 1 %Target Found
            if mean_difference>mean_thresh && entropy_difference>entropy_thresh_partial
                if entropy_difference<entropy_thresh_full
                    response_flag=2;
                else
                    response_flag=3;
                end
            end
        case 2 %Partial Loss
            if mean_difference<=mean_thresh || entropy_difference<=entropy_thresh_partial
                response_flag=1;
            elseif entropy_difference>=entropy_thresh_full
                response_flag=3;
            end
        case 3 %Full Loss
            if mean_difference<=mean_thresh || entropy_difference<=entropy_thresh_partial
                response_flag=1;
            elseif entropy_difference<entropy_thresh_full % Go to partial loss mode
                response_flag=2;
            end
    end
    
    %Execute failure detector state operations
    switch response_flag
        case 1 %Target Found
            [vert_delta, horiz_delta] = find(response == max(response(:)), 1);
            vert_delta  = vert_delta  - floor(size(zf,1)/2);
            horiz_delta = horiz_delta - floor(size(zf,2)/2);
            pos = pos + cell_size * [vert_delta - 1, horiz_delta - 1]; % Map the position to the image space
        case 2 %Partial Loss
            [vert_delta, horiz_delta] = find(response == max(response(:)), 1);
            vert_delta  = vert_delta  - floor(size(zf,1)/2);
            horiz_delta = horiz_delta - floor(size(zf,2)/2);
            pos = pos + cell_size * [vert_delta - 1, horiz_delta - 1]; % Map the position to the image space
            mean_list=mean_list(2:end); %forget newest element in array
            entropy_list=entropy_list(2:end); %forget newest element in array
        case 3 %Full Loss
            mean_list=mean_list(2:end); %forget newest element in array
            entropy_list=entropy_list(2:end); %forget newest element in array
        case 0 % Init
            [vert_delta, horiz_delta] = find(response == max(response(:)), 1);
            vert_delta  = vert_delta  - floor(size(zf,1)/2);
            horiz_delta = horiz_delta - floor(size(zf,2)/2);
            pos = pos + cell_size * [vert_delta - 1, horiz_delta - 1]; % Map the position to the image space
            response_flag=1;
    end
    
    % Package data for RFD-CF2 debug visualization graphs
    visualPkg{1} = 1; %valid bit
    visualPkg{2} = response; %response mat
    visualPkg{3} = mean_difference/mean_thresh; %normalized mean
    visualPkg{4} = entropy_difference/entropy_thresh_full; %normalized entropy
end

function [model_xf, model_alphaf] = updateModel(feat, yf, interp_factor, lambda, frame, ...
    model_xf, model_alphaf)

    numLayers = length(feat);

    % ================================================================================
    % Initialization
    % ================================================================================
    xf       = cell(1, numLayers);
    alphaf   = cell(1, numLayers);

    % ================================================================================
    % Model update
    % ================================================================================
    for ii=1 : numLayers
        xf{ii} = fft2(feat{ii});
        kf = sum(xf{ii} .* conj(xf{ii}), 3) / numel(xf{ii});
        alphaf{ii} = yf./ (kf+ lambda);   % Fast training
    end

    % Model initialization or update
    if frame == 1   % First frame, train with a single image
        for ii=1:numLayers
            model_alphaf{ii} = alphaf{ii};
            model_xf{ii} = xf{ii};
        end
    else
        % Online model update using learning rate interp_factor
        for ii=1:numLayers
            model_alphaf{ii} = (1 - interp_factor) * model_alphaf{ii} + interp_factor * alphaf{ii};
            model_xf{ii}     = (1 - interp_factor) * model_xf{ii}     + interp_factor * xf{ii};
        end
    end
end

function feat  = extractFeature(im, pos, window_sz, cos_window, indLayers)

% Get the search window from previous detection
patch = get_subwindow(im, pos, window_sz);
% Extracting hierarchical convolutional features
feat  = get_features(patch, cos_window, indLayers);

end
