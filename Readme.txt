This is the research code and results for the paper:
		Walsh, R., Medeiros, H. “Detecting Tracking Failures From Correlation Response Maps,” International Symposium on Visual Computing, 2016.

I. Introduction
II. Quick Setup Guide
III. OBT-50 and OBT-100 Interface Setup
IV. VOT2015 and VOT2016 Interface Setup
V. Other useful information
VI. Citations and Contact


I. Introduction
	- CF2 with Response Information Failure Detection (RFD-CF2) is a modified version of the Correlation Filters with Convolutional Features tracker (CF2) extended with a failure detection module.
	- This document and its instructions for setup assumes that you are using a Linux environment with MATLAB.


II. Quick Setup Guide

	1. Download the mat file for VGG-NET-19 and place it in the model folder
		- Using link: https://uofi.box.com/shared/static/kxzjhbagd6ih1rf7mjyoxn2hy70hltpl.mat
			- Or link if in China: http://pan.baidu.com/s/1kU1Me5T
		- Note that this mat file is compatible with the MatConvNet-1beta20 used in this work. 
			If you download the mat file from http://www.vlfeat.org/matconvnet/models/imagenet-vgg-verydeep-19.mat, 
			please pay attention to the version compatibility.

	2. Download MatConvNet and unpack to a directory of your choice
		- We used 1.0-beta20. If you are using another version, please ensure version compatability

	3. Setup MatCovNet by including the path of matconvnet/matlab
		- This should look something like (Full path may vary)
			addpath('~/SourceCode/matconvnet-1.0-beta20/matlab');
			vl_setupnn
			vl_compilenn

	4. Run the main entry file run_tracker.m
		- A window will appear asking you which sequence you want to select. 
			- Select it and the the sequence with debug info should run.
		- If this works, congratulations!


III. OBT-50 and OBT-100 Interface Setup

	1. Download the Visual Tracking Benchmark and unpack to a directory of your choice
		- Using link: http://cvlab.hanyang.ac.kr/tracker_benchmark

	2. Add the RFD-CF2 tracker folder to the benchmark's tracking folder

	3. Update the configTrackers.m file inside the util folder
		- Add RFD-CF2 to a tracker set that you desire to benchmark against
			- This should look something like
				struct('name', 'RFD_CF2', 'namePaper', 'RFD-CF2'), ...
			- make sure that that your benchmarking set is added to the trackers variable in the last line
	
	4. Run It
		- Run main_running.m to run the benchmark
			- You should see the benchmark updating with a visual window
		- Run perfPlot.m to to generate graphs

IV. VOT2015 and VOT2016 Interface Setup

	Known Issue:
	The run_RFD_CF2_VOT file does not generate the same results as the paper. Adding comments, changing variable names, 
	and changing the filename appears to change results with consistency. The results we provide are the same for the paper, 
	and the original, uncommented code for the VOT implementation is included in the folder, originalFiles.

	1. Download the VOT Benchmark and unpack to a directory of your choice using instructions
		- Using link: http://www.votchallenge.net/howto
		- We named our tracker RFD_CF2
		- The tracker interpreter is matlab
	
	2. Add and configure the RFD-CF2 tracker folder inside your VOT workspace folder as given in the VOT tutorial
		- Add the vot.m file from the tutorial into the RFD-CF2 tracker folder as the tutorial suggests
	
	3. Configure the tracker_RFD_CF2.m file
		- Remove the error warning line
		- set the tracker configuration to the following (Full paths may vary)
			tracker_command = generate_matlab_command('tracker_ensemble',...
			   {'~/Documents/MATLAB/VOT2016/vot-workspace/tracker_folder_RFD_CF2',...
				'~/Documents/MATLAB/VOT2016/vot-workspace/tracker_folder_RFD_CF2/utility',...
				'~/Documents/MATLAB/VOT2016/vot-workspace/tracker_folder_RFD_CF2/model',...
				'~/Documents/MATLAB/VOT2016/vot-workspace/tracker_folder_RFD_CF2/external/matconvnet/matlab',...
				'~/Documents/MATLAB/VOT2016/vot-workspace/tracker_folder_RFD_CF2/external/matconvnet/matlab/mex',...
				'~/Documents/MATLAB/VOT2016/vot-workspace/tracker_folder_RFD_CF2/external/matconvnet/matlab/xtest'});
	
	4. Run a test via run_test.m
		- Select Run tracker once within the evaluation and any sequence you desire
		- The program should, after a while, give you the option to Visually compare results with the groundtruth


V. Other useful information

	- Note that not all included files have been modified from source. The new and or modified files include:
		run_RFD_CF2.m
		run_RFD_CF2_VOT.m
		run_tracker.m
		tracker_ensemble.m
		show_video.m
	- run_RFD_CF2 is an interfacce for OBT-50 and OBT-100
	- run_RFD_CF2_VOT is an interface for VOT2015 and VOT2016
	- run_tracker is the standard tracker run file


VI. Citations and Contact

	CF2 Citation:
		Hierarchical Convolutional Features for Visual Tracking
		Chao Ma, Jia-Bin Huang, Xiaokang Yang, and Ming-Hsuan Yang
		IEEE International Conference on Computer Vision, ICCV 2015

	Our Paper:
		Detecting Tracking Failures From Correlation Response Maps
		Walsh, Ryan and Medeiros, Henry	
		International Symposium on Visual Computing, ISVC 2016

	Contact For CF2 Base Code:
		Chao Ma (chaoma99@gmail.com), or
		Jia-Bin Huang (jbhuang1@illinois.edu)

	Contact For RFD-CF2:
		Ryan Walsh (ryan.w.walsh@marquette.edu), or
		Henry Medeiors (henry.medeiros@marquette.edu)
