function update_visualization_func = show_video(img_files, video_path, entropy_thresh_full, entropy_thresh_partial, debug_bool)
%SHOW_VIDEO
%   Visualizes a tracker in an interactive figure, given a cell array of
%   image file names, their path, and whether to resize the images to
%   half size or not.
%
%   This function returns an UPDATE_VISUALIZATION function handle, that
%   can be called with a frame number and a bounding box [x, y, width,
%   height], as soon as the results for a new frame have been calculated.
%   This way, your results are shown in real-time, but they are also
%   remembered so you can navigate and inspect the video afterwards.
%   Press 'Esc' to send a stop signal (returned by UPDATE_VISUALIZATION).
%
%   Joao F. Henriques, 2014
%   http://www.isr.uc.pt/~henriques/
%   
%   This function was updated by Ryan Walsh
%   ryan.w.walsh@marquette.edu

	%store one instance per frame
	num_frames = numel(img_files);
	boxes = cell(num_frames,1);
    boxes_sa = cell(num_frames,1);
    
    % RFD-CF2 store one instance per frame
    colors = cell(num_frames,1);
    responses = cell(num_frames,1);
    normalized_means = zeros(1, num_frames);
    normalized_entropies = zeros(1, num_frames);

	%create window
	[fig_h, axes_h, unused, scroll] = videofig(num_frames, @redraw, [], [], @on_key_press);  %#ok, unused outputs
	set(fig_h, 'Name', ['Tracker - ' video_path])
	axis off;
	
	%image, rectangle, and plot, handles start empty, they are initialized later
	im_h = [];
	rect_h = [];
	
	update_visualization_func = @update_visualization;
	stop_tracker = false;
    
	function stop = update_visualization(frame, box, color, visualPkg)
		%store the tracker instance for one frame, and show it. returns
		%true if processing should stop (user pressed 'Esc').
		boxes{frame} = box;
        colors{frame} = color;
        responses{frame} = visualPkg{2};
        if(visualPkg{1})
            normalized_means(frame) = visualPkg{3};
            normalized_entropies(frame) = visualPkg{4};
        end
        boxes_sa{frame} = visualPkg{5};
		scroll(frame);
		stop = stop_tracker;
	end

	function redraw(frame)
		%render main image
		im = imread([video_path img_files{frame}]);
        
        if debug_bool
            if isempty(im_h)  %create image
                tmp_fig = figure('visible', 'off'); 
                set(tmp_fig, 'Position', [0, 0, 640, 480], 'Units','normalized');
                subplot(8,8,1:48);
                imshow(im, 'Border','tight', 'InitialMag',200);
                fig_frm = getframe(gcf);
                fig_img = frame2im(fig_frm);
                close(tmp_fig);
                im_h = imshow(fig_img, 'Border', 'tight', 'InitialMag', 200, 'Parent',axes_h);
            else  %just update it
                tmp_fig = figure('visible', 'off'); 
                set(tmp_fig, 'Position', [0, 0, 640, 480], 'Units','normalized');
                subplot(8,8,1:48);
                imshow(im, 'Border','tight', 'InitialMag',200);
                if ~isempty(boxes_sa{frame})
                    rectangle('Position', boxes_sa{frame}, 'EdgeColor', 'black', 'LineWidth',1.4);
                end
                if ~isempty(boxes{frame})
                    rectangle('Position', boxes{frame}, 'EdgeColor', colors{frame}, 'LineWidth',1.4);
                end
                if size(responses{frame}, 1) > 1
                    subplot(8,8,[49:51, 57:59]);
                    imagesc(responses{frame}, [-.1, 1]);
                    colorbar();
                    set(gca,'Visible','off');
                    set(gca,'Ytick',[])
                    set(gca,'Yticklabel',[])
                    set(gca,'Xtick',[])
                    set(gca,'Xticklabel',[])
                    subplot(8,8,[52:56, 60:64]);

                    srt = frame-20;
                    fin = frame;
                    if srt < 1
                        srt = 1;
                    end
                    
                    t = srt:fin;
                    plot(t, -normalized_means(t), 'm', t, -normalized_entropies(t), 'b',t,t*0,'--g', t,t*0-entropy_thresh_partial/entropy_thresh_full,'--c', t,t*0-1,'--r', 'LineWidth',1.4);
                    axis([srt, fin, -1.5 0.5]);
                    set(gca, 'Ytick', []);
                end
                fig_frm = getframe(gcf);
                fig_img = frame2im(fig_frm);
                close(tmp_fig);
                set(im_h, 'CData', fig_img);
            end
        else
            if isempty(im_h)  %create image
                im_h = imshow(im, 'Border','tight', 'InitialMag',200, 'Parent',axes_h);
            else  %just update it
                set(im_h, 'CData', im)
            end
            
            %render target search area box for this frame
            if isempty(rect_sa_h)  %create it for the first time
                rect_sa_h = rectangle('Position',[0,0,1,1], 'EdgeColor', 'black', 'Parent',axes_h);
            end
            
            %render target bounding box for this frame
            if isempty(rect_h)  %create it for the first time
                rect_h = rectangle('Position',[0,0,1,1], 'EdgeColor', colors{frame}, 'Parent',axes_h);
            end

            if ~isempty(boxes{frame})
                set(rect_h, 'Visible', 'on', 'Position', boxes{frame}, 'EdgeColor', colors{frame});
            else
                set(rect_h, 'Visible', 'off');
            end
            
        end     
	end

	function on_key_press(key)
		if strcmp(key, 'escape')  %stop on 'Esc'
			stop_tracker = true;
		end
	end

end

