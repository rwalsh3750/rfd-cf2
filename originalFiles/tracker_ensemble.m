% tracker_ensemble: Correlation filter tracking with convolutional features
%
% Input:
%   - video_path:          path to the image sequence
%   - img_files:           list of image names
%   - pos:                 intialized center position of the target in (row, col)
%   - target_sz:           intialized target size in (Height, Width)
% 	- padding:             padding parameter for the search area
%   - lambda:              regularization term for ridge regression
%   - output_sigma_factor: spatial bandwidth for the Gaussian label
%   - interp_factor:       learning rate for model update
%   - cell_size:           spatial quantization level
%   - show_visualization:  set to True for showing intermediate results
%   - maxLstSze:           raise to prevent tracker from learning too fast
%   - meanThresh:          mean thresholding between (any loss/found)
%   - entropyThresh:       entropy threshold between (partial loss/full loss)
%   - entropyThresh2:      entropy threshold between (partial loss/found)
% Output:
%   - positions:           predicted target position at each frame
%   - time:                time spent for tracking
%
%   It is provided for educational/researrch purpose only.
%   If you find the software useful, please consider cite our paper.
%
%   Hierarchical Convolutional Features for Visual Tracking
%   Chao Ma, Jia-Bin Huang, Xiaokang Yang, and Ming-Hsuan Yang
%   IEEE International Conference on Computer Vision, ICCV 2015
%
% Contact:
%   Chao Ma (chaoma99@gmail.com), or
%   Jia-Bin Huang (jbhuang1@illinois.edu).


function tracker_ensemble

cleanup = onCleanup(@() exit());
[handle, image, region] = vot('rectangle');

% Image file names
target_sz = [region(4), region(3)];
pos = [region(2), region(1)] + floor(target_sz/2);
paddingOriginal = struct('generic', 1.8, 'large', 1, 'height', 0.4);

lambda = 1e-4;              % Regularization
output_sigma_factor = 0.1;  % Spatial bandwidth (proportional to target)

interp_factor = 0.01;       % Model learning rate
cell_size = 4;              % Spatial cell size

maxLstSze = 30;             % Adjusts alowable limit for learning rate 
meanThresh = 0.4;           % Adjusts the mean threshold for seeing the target
entropyThresh = 0.53;       % Adjusts the entropy threshold for completly loosing the target
entropyThresh2 = 0.3;       % Adjusts the entropy threshold for partially loosing the target
maxMult = 2;                % Adjusts maximum search area scaling
minMult = 0.96;             % Adjusts minimum search area scaling
padding = paddingOriginal;

% ================================================================================
% Environment setting
% ================================================================================

indLayers = [37, 28, 19];   % The CNN layers Conv5-4, Conv4-4, and Conv3-4 in VGG Net
nweights  = [1, 0.5, 0.02]; % Weights for combining correlation filter responses
numLayers = length(indLayers);

% Get image size and search window size
im_sz     = size(imread(image));
window_sz = get_search_window(target_sz, im_sz, padding);

% Compute the sigma for the Gaussian function label
output_sigma = sqrt(prod(target_sz)) * output_sigma_factor / cell_size;

%create regression labels, gaussian shaped, with a bandwidth
%proportional to target size    d=bsxfun(@times,c,[1 2]);

l1_patch_num = floor(window_sz/ cell_size);

% Pre-compute the Fourier Transform of the Gaussian function label
yf = fft2(gaussian_shaped_labels(output_sigma, l1_patch_num));

% Pre-compute and cache the cosine window (for avoiding boundary discontinuity)
cos_window = hann(size(yf,1)) * hann(size(yf,2))';

% Initialize variables for calculating FPS and distance precision
nweights  = reshape(nweights,1,1,[]);

% Note: variables ending with 'f' are in the Fourier domain.
model_xf     = cell(1, numLayers);
model_alphaf = cell(1, numLayers);

%Declare variables for use in occlusion detection
meanLst=[];
entropyLst=[];
responseFlag=0;
multScale = maxMult-minMult;
firstFrame = true;

% ================================================================================
% Start tracking
% ================================================================================
while true,
    
    if ~firstFrame
        [handle, image] = handle.frame(handle);
    end
    
    if isempty(image)
        break;
    end;
    
    im = imread(image);
    
    if ismatrix(im)
        im = cat(3, im, im, im);
    end
    
    % ================================================================================
    % Predicting the object position from the learned object model
    % ================================================================================
    if ~firstFrame
        % Extracting hierarchical convolutional features
        feat = extractFeature(im, pos, window_sz, cos_window, indLayers);
        % Predict position
        [pos, meanLst, entropyLst, responseFlag, padding]  = predictPosition(feat, pos, indLayers, nweights, cell_size, l1_patch_num, ...
            model_xf, model_alphaf, meanLst, entropyLst, maxLstSze, meanThresh, entropyThresh, entropyThresh2, responseFlag, paddingOriginal, minMult, multScale);
        window_sz = get_search_window(target_sz, im_sz, padding);
    end
    
    % ================================================================================
    % Learning correlation filters over hierarchical convolutional features
    % ================================================================================
    % Extracting hierarchical convolutional features
    feat  = extractFeature(im, pos, window_sz, cos_window, indLayers);
    % Model update
    
    if responseFlag == 1 || responseFlag==0,
    [model_xf, model_alphaf] = updateModel(feat, yf, interp_factor, lambda, firstFrame, ...
        model_xf, model_alphaf);
    end

    % ================================================================================
    % Save predicted position and timing
    % ================================================================================
    if firstFrame
        firstFrame = false;
    else
        region = [pos(2)-target_sz(2)/2, pos(1)-target_sz(1)/2, target_sz(2), target_sz(1)];
        handle = handle.report(handle, region);
    end
    
end
    handle.quit(handle);
end



function [pos, meanLst, entropyLst, responseFlag, padding] = predictPosition(feat, pos, indLayers, nweights, cell_size, l1_patch_num, ...
    model_xf, model_alphaf, meanLst, entropyLst, maxLstSze, meanThresh, entropyThresh, entropyThresh2, responseFlag, paddingOriginal, minMult, multScale)

    % ================================================================================
    % Compute correlation filter responses at each layer
    % ================================================================================
    res_layer = zeros([l1_patch_num, length(indLayers)]);

    for ii = 1 : length(indLayers)
        zf = fft2(feat{ii});
        kzf=sum(zf .* conj(model_xf{ii}), 3) / numel(zf);

        res_layer(:,:,ii) = real(fftshift(ifft2(model_alphaf{ii} .* kzf)));  %equation for fast detection
    end
    
    % Combine responses from multiple layers (see Eqn. 5)
    response = sum(bsxfun(@times, res_layer, nweights), 3);
    
    
    % ================================================================================
    % Find target location
    % ================================================================================
    % Target location is at the maximum response. we must take into
    % account the fact that, if the target doesn't move, the peak
    % will appear at the top-left corner, not at the center (this is
    % discussed in the KCF paper). The responses wrap around cyclically.

     responseMean= mean(response(:));
     responseEntropy = entropy(imadjust(response(:)));
    
    %Find weighted averages
    meanLst=[responseMean, meanLst];
    while size(meanLst, 2)>maxLstSze+1,
        meanLst=meanLst(1:end-1);
    end
    meanWtAvg=0;
    total=0;
    for i=1:1:size(meanLst, 2),
        meanWtAvg=meanWtAvg+meanLst(i)*i;
        total=total+i;
    end
    meanWtAvg=meanWtAvg/total;
    meanDiff = (meanWtAvg-responseMean)/((meanWtAvg+responseMean)/2);
    
    entropyLst=[responseEntropy, entropyLst];
    while size(entropyLst, 2)>maxLstSze+1,
        entropyLst=entropyLst(1:end-1);
    end
    entropyWtAvg=0;
    total=0;
    for i=1:1:size(entropyLst, 2),
        entropyWtAvg=entropyWtAvg+entropyLst(i)*i;
        total=total+i;
    end
    entropyWtAvg=entropyWtAvg/total;
    entropyDiff = -(entropyWtAvg-responseEntropy)/((entropyWtAvg+responseEntropy)/2);
    
    meanMult = meanDiff/meanThresh;
    if meanMult<0
        meanMult=0;
    elseif meanMult>1
        meanMult=1;
    end

    entropyMult = entropyDiff/entropyThresh;
    if entropyMult<0
        entropyMult=0;
    elseif meanMult>1
        entropyMult=1;
    end

    %mult=multScale*((meanDiff+entropyDiff)/2)*sqrt(entropyMult*meanMult)+minMult;
    mult=multScale*((meanDiff/meanThresh+entropyDiff/entropyThresh)/2)*sqrt(entropyMult*meanMult)+minMult;
    
    if mult>(minMult+multScale),
        mult=minMult+multScale;
    elseif mult<minMult,
        mult=minMult;
    end
        
    padding = struct('generic', paddingOriginal.generic*mult, 'large', paddingOriginal.large*mult, 'height', paddingOriginal.height*mult);
    
    %Find correct state
    switch responseFlag
        case 1 %Target faound
            if meanDiff>meanThresh && entropyDiff>entropyThresh2,
                if entropyDiff<entropyThresh,
                    responseFlag=2;
                else
                    responseFlag=3;
                end
            end
        case 2 %Partial Loss
            if meanDiff<=meanThresh || entropyDiff<=entropyThresh2,
                responseFlag=1;
            elseif entropyDiff>=entropyThresh,
                responseFlag=3;
            end
        case 3 %Full Loss
            if meanDiff<=meanThresh || entropyDiff<=entropyThresh2,
                responseFlag=1;
            elseif entropyDiff<entropyThresh, % Go to partial loss mode
                responseFlag=2;
            end
    end
    
    %Execute state operations
    switch responseFlag
        case 1 %Target faound
            [vert_delta, horiz_delta] = find(response == max(response(:)), 1);
            vert_delta  = vert_delta  - floor(size(zf,1)/2);
            horiz_delta = horiz_delta - floor(size(zf,2)/2);
            pos = pos + cell_size * [vert_delta - 1, horiz_delta - 1]; % Map the position to the image space
        case 2 %Partial Loss
            [vert_delta, horiz_delta] = find(response == max(response(:)), 1);
            vert_delta  = vert_delta  - floor(size(zf,1)/2);
            horiz_delta = horiz_delta - floor(size(zf,2)/2);
            pos = pos + cell_size * [vert_delta - 1, horiz_delta - 1]; % Map the position to the image space
            meanLst=meanLst(2:end); %forget newest element in array
            entropyLst=entropyLst(2:end); %forget newest element in array
        case 3 %Full Loss
            meanLst=meanLst(2:end); %forget newest element in array
            entropyLst=entropyLst(2:end); %forget newest element in array
        case 0 % Init
            [vert_delta, horiz_delta] = find(response == max(response(:)), 1);
            vert_delta  = vert_delta  - floor(size(zf,1)/2);
            horiz_delta = horiz_delta - floor(size(zf,2)/2);
            pos = pos + cell_size * [vert_delta - 1, horiz_delta - 1]; % Map the position to the image space
            responseFlag=1;
    end
    
end

function [model_xf, model_alphaf] = updateModel(feat, yf, interp_factor, lambda, firstFrame, ...
    model_xf, model_alphaf)

    numLayers = length(feat);

    % ================================================================================
    % Initialization
    % ================================================================================
    xf       = cell(1, numLayers);
    alphaf   = cell(1, numLayers);

    % ================================================================================
    % Model update
    % ================================================================================
    for ii=1 : numLayers
        xf{ii} = fft2(feat{ii});
        kf = sum(xf{ii} .* conj(xf{ii}), 3) / numel(xf{ii});
        alphaf{ii} = yf./ (kf+ lambda);   % Fast training
    end

    % Model initialization or update
    if firstFrame,  % First frame, train with a single image
        for ii=1:numLayers
            model_alphaf{ii} = alphaf{ii};
            model_xf{ii} = xf{ii};
        end
    else
        % Online model update using learning rate interp_factor
        for ii=1:numLayers
            model_alphaf{ii} = (1 - interp_factor) * model_alphaf{ii} + interp_factor * alphaf{ii};
            model_xf{ii}     = (1 - interp_factor) * model_xf{ii}     + interp_factor * xf{ii};
        end
    end
end

function feat  = extractFeature(im, pos, window_sz, cos_window, indLayers)

% Get the search window from previous detection
patch = get_subwindow(im, pos, window_sz);
% Extracting hierarchical convolutional features
feat  = get_features(patch, cos_window, indLayers);

end
