% run_RFD_CF2: RFD-CF2
%
% Input:
%     - seq:        sequence name
%     - res_path:   result path
%     - bSaveImage: flag for saving images
% Output:
%     - results: tracking results, position prediction over time
%
%   It is provided for educational/research purposes only.
%   If you find the software useful, please consider citing our paper.
%
% Citations and References:
%   CF2 Citation:
%   Hierarchical Convolutional Features for Visual Tracking
%   Chao Ma, Jia-Bin Huang, Xiaokang Yang, and Ming-Hsuan Yang
%   IEEE International Conference on Computer Vision, ICCV 2015
%
%   Our Paper:
%	Detecting Tracking Failures From Correlation Response Maps
%	Walsh, Ryan and Medeiros, Henry	
%	International Symposium on Visual Computing, ISVC 2016
%
%   Contact For CF2 Base Code:
%   Chao Ma (chaoma99@gmail.com), or
%   Jia-Bin Huang (jbhuang1@illinois.edu)
%
%   Contact For RFD-CF2:
%   Ryan Walsh (ryan.w.walsh@marquette.edu), or
%   Henry Medeiors (henry.medeiros@marquette.edu)

function results = run_RFD_CF2(seq, res_path, bSaveImage)

% ================================================================================
% Environment setting
% ================================================================================

% Image file names
img_files = seq.s_frames;
% Seletected target size
target_sz = [seq.init_rect(1,4), seq.init_rect(1,3)];
% Initial target position
pos = [seq.init_rect(1,2), seq.init_rect(1,1)] + floor(target_sz/2);

% Extra area surrounding the target for including contexts
padding_original = struct('generic', 1.8, 'large', 1, 'height', 0.4);

lambda = 1e-4;                      % Regularization
output_sigma_factor = 0.1;          % Spatial bandwidth (proportional to target)

interp_factor = 0.01;               % Model learning rate
cell_size = 4;                      % Spatial cell size

max_list_size = 30;                 % Adjusts allowable limit for learning rate 
mean_thresh = 0.4;                  % Adjusts the mean threshold for seeing the target
entropy_thresh_full = 0.53;         % Adjusts the entropy threshold for completely loosing the target
entropy_thresh_partial = 0.3;       % Adjusts the entropy threshold for partially loosing the target
scale_max = 2;                      % Adjusts maximum search area scaling
scale_min = 0.96;                   % Adjusts minimum search area scaling

video_path = '';                    %For running in tracker_benchmark_v1.0, set to '../.' when placing tracker in trackers folder

show_visualization = false;
debug_level = false;

% ================================================================================
% Main entry function for visual tracking
% ================================================================================
[positions, time] = tracker_ensemble(video_path, img_files, pos, target_sz, ...
            lambda, output_sigma_factor, interp_factor, ...
            cell_size, show_visualization, debug_level, max_list_size, mean_thresh, entropy_thresh_full, entropy_thresh_partial, padding_original, scale_min, scale_max);

% ================================================================================
% Return results to benchmark, in a workspace variable
% ================================================================================
rects      = [positions(:,2) - target_sz(2)/2, positions(:,1) - target_sz(1)/2];
rects(:,3) = target_sz(2);
rects(:,4) = target_sz(1);
results.type   = 'rect';
results.res    = rects;
results.fps    = numel(img_files)/time;

end