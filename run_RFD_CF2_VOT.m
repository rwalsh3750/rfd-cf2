% run_RFD_CF2_VOT: RFD-CF2
%
% Output:
%   - positions: predicted target position at each frame
%
%   It is provided for educational/research purposes only.
%   If you find the software useful, please consider citing our paper.
%
% Known Issue: 
%	The run_RFD_CF2_VOT file does not generate the same results as the paper.
%	Adding comments, changing variable names, and changing the filename appears to change results with consistency. 
%	The results we provide are the same for the paper, and the original, uncommented code for the VOT implementation 
%	is included in the folder, originalFiles.
%
% Citations and References:
%   CF2 Citation:
%   Hierarchical Convolutional Features for Visual Tracking
%   Chao Ma, Jia-Bin Huang, Xiaokang Yang, and Ming-Hsuan Yang
%   IEEE International Conference on Computer Vision, ICCV 2015
%
%   Our Paper:
%	Detecting Tracking Failures From Correlation Response Maps
%	Walsh, Ryan and Medeiros, Henry	
%	International Symposium on Visual Computing, ISVC 2016
%
%   Contact For CF2 Base Code:
%   Chao Ma (chaoma99@gmail.com), or
%   Jia-Bin Huang (jbhuang1@illinois.edu)
%
%   Contact For RFD-CF2:
%   Ryan Walsh (ryan.w.walsh@marquette.edu), or
%   Henry Medeiors (henry.medeiros@marquette.edu)

function run_RFD_CF2_VOT

cleanup = onCleanup(@() exit());
[handle, image, region] = vot('rectangle');

% Image file names
target_sz = [region(4), region(3)];
pos = [region(2), region(1)] + floor(target_sz/2);
padding_original = struct('generic', 1.8, 'large', 1, 'height', 0.4);

lambda = 1e-4;                  % Regularization
output_sigma_factor = 0.1;      % Spatial bandwidth (proportional to target)

interp_factor = 0.01;           % Model learning rate
cell_size = 4;                  % Spatial cell size

max_list_size = 30;             % Adjusts allowable limit for learning rate 
mean_thresh = 0.4;              % Adjusts the mean threshold for seeing the target
entropy_thresh_full = 0.53;     % Adjusts the entropy threshold for completly loosing the target
entropy_thresh_partial = 0.3;   % Adjusts the entropy threshold for partially loosing the target
scale_max = 2;                  % Adjusts maximum search area scaling
scale_min = 0.96;               % Adjusts minimum search area scaling
padding = padding_original;

% ================================================================================
% Environment setting
% ================================================================================

indLayers = [37, 28, 19];   % The CNN layers Conv5-4, Conv4-4, and Conv3-4 in VGG Net
nweights  = [1, 0.5, 0.02]; % Weights for combining correlation filter responses
numLayers = length(indLayers);

% Get image size and search window size
im_sz     = size(imread(image));
window_sz = get_search_window(target_sz, im_sz, padding);

% Compute the sigma for the Gaussian function label
output_sigma = sqrt(prod(target_sz)) * output_sigma_factor / cell_size;

%create regression labels, gaussian shaped, with a bandwidth
%proportional to target size    d=bsxfun(@times,c,[1 2]);

l1_patch_num = floor(window_sz/ cell_size);

% Pre-compute the Fourier Transform of the Gaussian function label
yf = fft2(gaussian_shaped_labels(output_sigma, l1_patch_num));

% Pre-compute and cache the cosine window (for avoiding boundary discontinuity)
cos_window = hann(size(yf,1)) * hann(size(yf,2))';

% Initialize variables for calculating FPS and distance precision
nweights  = reshape(nweights,1,1,[]);
    
% Note: variables ending with 'f' are in the Fourier domain.
model_xf     = cell(1, numLayers);
model_alphaf = cell(1, numLayers);

%Declare variables for use in occlusion detection
mean_list       = [];
entropy_list    = [];
response_flag   = 0;
scale_range     = scale_max-scale_min;
first_frame     = true;

% ================================================================================
% Start tracking
% ================================================================================
while true
    
    if ~first_frame %Grab first frame from VOT interface
        [handle, image] = handle.frame(handle);
    end
    
    if isempty(image) %If there is no next frame, break this loop
        break;
    end;
    
    im = imread(image); %Read next image
    
    if ismatrix(im)
        im = cat(3, im, im, im);
    end
    
    % ================================================================================
    % Predicting the object position from the learned object model
    % ================================================================================
    if ~first_frame
        % Extracting hierarchical convolutional features
        feat = extractFeature(im, pos, window_sz, cos_window, indLayers);
        % Predict position
        [pos, mean_list, entropy_list, response_flag, padding]  = predictPosition(feat, pos, indLayers, nweights, cell_size, l1_patch_num, ...
            model_xf, model_alphaf, mean_list, entropy_list, max_list_size, mean_thresh, entropy_thresh_full, entropy_thresh_partial, response_flag, padding_original, scale_min, scale_range);
        window_sz = get_search_window(target_sz, im_sz, padding);
    end
    
    % ================================================================================
    % Learning correlation filters over hierarchical convolutional features
    % ================================================================================
    % Extracting hierarchical convolutional features
    feat  = extractFeature(im, pos, window_sz, cos_window, indLayers);
    
    % Model update
    if response_flag == 1 || response_flag==0
    [model_xf, model_alphaf] = updateModel(feat, yf, interp_factor, lambda, first_frame, ...
        model_xf, model_alphaf);
    end

    % ================================================================================
    % Save predicted position and timing
    % ================================================================================
    if first_frame
        first_frame = false;
    else %Save bounding box position and send to VOT interface
        region = [pos(2)-target_sz(2)/2, pos(1)-target_sz(1)/2, target_sz(2), target_sz(1)];
        handle = handle.report(handle, region);
    end
    
end
    handle.quit(handle); %Close VOT interface
end



function [pos, mean_list, entropy_list, response_flag, padding] = predictPosition(feat, pos, indLayers, nweights, cell_size, l1_patch_num, ...
    model_xf, model_alphaf, mean_list, entropy_list, max_list_size, mean_thresh, entropy_thresh_full, entropy_thresh_partial, response_flag, padding_original, scale_min, scale_range)

    % ================================================================================
    % Compute correlation filter responses at each layer
    % ================================================================================
    res_layer = zeros([l1_patch_num, length(indLayers)]);

    for ii = 1 : length(indLayers)
        zf = fft2(feat{ii});
        kzf=sum(zf .* conj(model_xf{ii}), 3) / numel(zf);

        res_layer(:,:,ii) = real(fftshift(ifft2(model_alphaf{ii} .* kzf)));  %equation for fast detection
    end
    
    % Combine responses from multiple layers (see Eqn. 5)
    response = sum(bsxfun(@times, res_layer, nweights), 3);
    
    % ================================================================================
    % Find target location and Failure Detection
    % ================================================================================
    % Target location is at the maximum response. we must take into
    % account the fact that, if the target doesn't move, the peak
    % will appear at the top-left corner, not at the center (See KCF paper cited in CF2).
    % The responses wrap around cyclically.
    %
    % Failure detection is accomplished via computing a mean and entropy for
    % this response and thresholding them. The result of failure detection
    % is that the search space for computing the next response is altered,
    % online learning is toggled, and repositioning of the target is	    
    % toggled. (See RFD-CF2 paper)

    %Prepare and compute variables for failure detector
    response_mean = mean(response(:));
    response_entropy = entropy(imadjust(response(:)));
    
    %Find Mean Moving Average
    mean_list=[response_mean, mean_list];
    while size(mean_list, 2)>max_list_size+1
        mean_list=mean_list(1:end-1);
    end
    mean_moving_average=0;
    total=0;
    for i=1:1:size(mean_list, 2)
        mean_moving_average=mean_moving_average+mean_list(i)*i;
        total=total+i;
    end
    mean_moving_average=mean_moving_average/total;
    mean_difference = (mean_moving_average-response_mean)/((mean_moving_average+response_mean)/2);
    
    %Find Entropy Moving Average
    entropy_list=[response_entropy, entropy_list];
    while size(entropy_list, 2)>max_list_size+1
        entropy_list=entropy_list(1:end-1);
    end
    entropy_moving_average=0;
    total=0;
    for i=1:1:size(entropy_list, 2)
        entropy_moving_average=entropy_moving_average+entropy_list(i)*i;
        total=total+i;
    end
    entropy_moving_average=entropy_moving_average/total;
    entropy_difference = -(entropy_moving_average-response_entropy)/((entropy_moving_average+response_entropy)/2);
    
    mean_scaling_factor = mean_difference/mean_thresh;
    if mean_scaling_factor<0
        mean_scaling_factor=0;
    elseif mean_scaling_factor>1
        mean_scaling_factor=1;
    end

    entropy_scaling_factor = entropy_difference/entropy_thresh_full;
    if entropy_scaling_factor<0
        entropy_scaling_factor=0;
    elseif mean_scaling_factor>1
        entropy_scaling_factor=1;
    end

    scaling=scale_range*((mean_difference/mean_thresh+entropy_difference/entropy_thresh_full)/2)*sqrt(entropy_scaling_factor*mean_scaling_factor)+scale_min;
    
    if scaling>(scale_min+scale_range)
        scaling=scale_min+scale_range;
    elseif scaling<scale_min
        scaling=scale_min;
    end
    
    % Set new window scaling
    padding = struct('generic', padding_original.generic*scaling, 'large', padding_original.large*scaling, 'height', padding_original.height*scaling);
    
    %Find failure detector state
    switch response_flag
        case 1 %Target Found
            if mean_difference>mean_thresh && entropy_difference>entropy_thresh_partial
                if entropy_difference<entropy_thresh_full
                    response_flag=2;
                else
                    response_flag=3;
                end
            end
        case 2 %Partial Loss
            if mean_difference<=mean_thresh || entropy_difference<=entropy_thresh_partial
                response_flag=1;
            elseif entropy_difference>=entropy_thresh_full
                response_flag=3;
            end
        case 3 %Full Loss
            if mean_difference<=mean_thresh || entropy_difference<=entropy_thresh_partial
                response_flag=1;
            elseif entropy_difference<entropy_thresh_full % Go to partial loss mode
                response_flag=2;
            end
    end
    
    %Execute failure detector state operations
    switch response_flag
        case 1 %Target Found
            [vert_delta, horiz_delta] = find(response == max(response(:)), 1);
            vert_delta  = vert_delta  - floor(size(zf,1)/2);
            horiz_delta = horiz_delta - floor(size(zf,2)/2);
            pos = pos + cell_size * [vert_delta - 1, horiz_delta - 1]; % Map the position to the image space
        case 2 %Partial Loss
            [vert_delta, horiz_delta] = find(response == max(response(:)), 1);
            vert_delta  = vert_delta  - floor(size(zf,1)/2);
            horiz_delta = horiz_delta - floor(size(zf,2)/2);
            pos = pos + cell_size * [vert_delta - 1, horiz_delta - 1]; % Map the position to the image space
            mean_list=mean_list(2:end); %forget newest element in array
            entropy_list=entropy_list(2:end); %forget newest element in array
        case 3 %Full Loss
            mean_list=mean_list(2:end); %forget newest element in array
            entropy_list=entropy_list(2:end); %forget newest element in array
        case 0 % Init
            [vert_delta, horiz_delta] = find(response == max(response(:)), 1);
            vert_delta  = vert_delta  - floor(size(zf,1)/2);
            horiz_delta = horiz_delta - floor(size(zf,2)/2);
            pos = pos + cell_size * [vert_delta - 1, horiz_delta - 1]; % Map the position to the image space
            response_flag=1;
    end
    
end

function [model_xf, model_alphaf] = updateModel(feat, yf, interp_factor, lambda, first_frame, ...
    model_xf, model_alphaf)

    numLayers = length(feat);

    % ================================================================================
    % Initialization
    % ================================================================================
    xf       = cell(1, numLayers);
    alphaf   = cell(1, numLayers);

    % ================================================================================
    % Model update
    % ================================================================================
    for ii=1 : numLayers
        xf{ii} = fft2(feat{ii});
        kf = sum(xf{ii} .* conj(xf{ii}), 3) / numel(xf{ii});
        alphaf{ii} = yf./ (kf+ lambda);   % Fast training
    end

    % Model initialization or update
    if first_frame  % First frame, train with a single image
        for ii=1:numLayers
            model_alphaf{ii} = alphaf{ii};
            model_xf{ii} = xf{ii};
        end
    else
        % Online model update using learning rate interp_factor
        for ii=1:numLayers
            model_alphaf{ii} = (1 - interp_factor) * model_alphaf{ii} + interp_factor * alphaf{ii};
            model_xf{ii}     = (1 - interp_factor) * model_xf{ii}     + interp_factor * xf{ii};
        end
    end
end

function feat = extractFeature(im, pos, window_sz, cos_window, indLayers)

% Get the search window from previous detection
patch = get_subwindow(im, pos, window_sz);
% Extracting hierarchical convolutional features
feat  = get_features(patch, cos_window, indLayers);

end
